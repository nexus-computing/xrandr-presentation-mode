#!/bin/bash

TRANSFORM=`query-screen-transform.py`
if [ -z $TRANSFORM ]; then
	exit -1
fi

echo "using transform $TRANSFORM"
xrandr --output HDMI-1 --auto --filter nearest --fb 3840x2160x0x0 --transform $TRANSFORM
