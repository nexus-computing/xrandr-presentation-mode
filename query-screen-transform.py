
#!/usr/bin/python3

import os

native_name = "eDP-1"
external_name = "HDMI-1"


class Display:
    def __init__(self, width, height, name):
        self.width = int(width)
        self.height = int(height)
        self.name = name

    def __str__(self) -> str:
        return f"{self.name}: {self.width}x{self.height}"

    def aspect(self):
        return self.width / self.height

    def projectTo(self, other):
        scale = 1
        dx = 0
        dy = 0

        if self.aspect() < other.aspect():
            #print("aspect < other")
            scale = self.height / other.height
            dx = -(other.width * scale - self.width)/2
            dy = 0
        elif (self.aspect() > other.aspect()):
            #print("aspect > other")
            scale = self.width / other.width
            dx = 0
            dy = -(other.height * scale - self.height)/2
        else:
            scale = self.width / other.width
            dx = 0
            dy = 0

        return f"{scale},0,{dx},0,{scale},{dy},0,0,1"


def main():
    native = Display(3840, 2160, native_name)
    external = None
    #external = Display(1280, 800, "HDMI-1")

    output = os.popen('xrandr').read()

    start = False
    target = ""
    for line in output.split("\n"):
        if external_name in line:
            start = True
            continue

        if(start and "+" in line):
            target = line
            break

    items = target.strip().split(" ")
    if len(items) > 0:
        if("x" in items[0]):
            res = items[0].split("x")
            if(len(res) != 2):
                return -1

            external = Display(res[0], res[1], external_name)

    if external == None:
        return -1


    res = native.projectTo(external)
    print(res)


if __name__ == "__main__":
    main()


#screen_count = d.screen_count()
#default_screen = d.get_default_screen()
# for screen in range(0,screen_count):
#    info = d.screen(screen)
#    print("Screen: %s. Default: %s" % (screen, screen==default_screen))
#    print("Width: %s, height: %s" % (info.width_in_pixels,info.height_in_pixels))
