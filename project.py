#!/usr/bin/python3

import os
import sys

native_name = "eDP-1"
external_name = "HDMI-1"

USAGE = """
usage: project.py <primary display> <secondary display>

project.py: calculates the xrandr transform and 
new framebuffer dimension to project <primary> display onto <secondary> display.
Requires xrandr do be installed.

for a list of displays use xrandr

examples usage:
project.py eDP-1 HDMI-1
"""
class Display:
    def __init__(self, width, height, name):
        self.width = int(width)
        self.height = int(height)
        self.name = name

    def __str__(self) -> str:
        return f"{self.name}: {self.width}x{self.height}"

    def aspect(self):
        return self.width / self.height

    def projectTo(self, other):
        scale = 1
        dx = 0
        dy = 0

        if self.aspect() < other.aspect():
            print("aspect < other")
            scale = self.height / other.height
            dx = -(other.width * scale - self.width)/2
            dy = 0
        elif (self.aspect() > other.aspect()):
            print("aspect > other")
            scale = self.width / other.width
            dx = 0
            dy = -(other.height * scale - self.height)/2
        else:
            scale = self.width / other.width
            dx = 0
            dy = 0

        return f"{scale},0,{dx},0,{scale},{dy},0,0,1"

def usage():
    print(USAGE)

def find_display(name):
    display = None

    output = os.popen('xrandr').read()

    start = False
    target = ""
    for line in output.split("\n"):
        if name in line:
            start = True
            continue

        if(start and "+" in line):
            target = line
            break

    items = target.strip().split(" ")
    if len(items) > 0:
        if("x" in items[0]):
            res = items[0].split("x")
            if(len(res) != 2):
                return -1

            display = Display(res[0], res[1], name)

    return display

def main(primary_name, secondary_name):
    primary = find_display(primary_name)
    secondary = find_display(secondary_name)

    if(primary == None):
        print(f"unable to find primary display (or resolution for) {primary_name}")
        return -1
    
    if(secondary == None):
        print(f"unable to find secondary display (or resolution for) {secondary_name}")
        return -2

    res = primary.projectTo(secondary)
    print(f"--transform {res} --fb {primary.width}x{primary.height}x0x0")


if __name__ == "__main__":
    if len(sys.argv) < 3:
        usage()
        sys.exit(-3)

    sys.exit(main(sys.argv[1], sys.argv[2]))