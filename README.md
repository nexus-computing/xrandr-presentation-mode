# `xrandr` Presentation Mode Mirror

When mirroring the primary display for a presentation or teaching it is often complicated to find matching resolution between the secondary display (projectors) and the primary one.

In the best case there is common denominator close to the native resolution of the primary display. Yet when using high dpi primary displays the scaling of UI components gets thrown off when changing the resolution.

Another approach to solve this problem is to select the native resolution of both displays and project the primary display onto the secondary with a transformation. `xrandr` supports exactly that yet is not always trivial to find the matching transformation parameters.

This python / bash script does exactly that.

## Goals

- The goal of presentation mode is to mirror the same content rendered on the primary display on a secondary display.
- All displays are rendered at their preferred resolution.
- The content of the primary display is scaled on the secondary display.
- If the aspect ratio of the secondary display does not match the one on the primary display, black bars are utilized to pad the content on the secondary display.

## Presenting the Screen with `present.sh`

```bash
present.sh <primary display> <secondary display>
```

Mirrors the primary display with the appropriate projection onto the secondary display

## How it works

![Goal: Mirror setup for primary and secondary Display](./img/mirror.svg)

In xrandr this is achieved by first scaling the width or height (depending on comparative aspect ratio) of the secondary display to the primary display and overlapping them.


xrandr keeps track of all displays and composes a virtual screen which includes all monitors. The the virtual display is also called framebuffer. By default the framebuffer will surround the union of all monitors composited on the virtual screen.

|            | primary     | secondary  |
| ---------- | ----------- | ---------- |
| name       | `eDP-1`     | `HDMI-1 `  |
| resolution | `3820x2160` | `1280x800` |


![xrandr virtual screen (framebuffer). Aspect rations don't match](./img/layout.svg)

In a first step the secondary display is scaled to entirely cover the primary display.

![xrandr: cover the primary diplay with the secondary](./img/cover.svg)

```bash
# primary display with navtive resolution
xrandr --auto --output eDP-1
# secondary display with native resolution
# --same-as draws display on top of eDP-1
# --transform applies a scaling transformation to cover the primary display
xrandr --auto --output HDMI-1 --same-as eDP-1 --transform 3.0,0,0,0,3.0,0,0,0,1
```

After covering the primary display the faramebuffer has grown to the extent of the secondary display.

Next, the framebuffer needs to be cropped and offset by half of the delta of the displays offset.

![xrandr: overlap displays](./img/overlap.svg)

```bash
# primary display with navtive resolution
xrandr --auto --output eDP-1

# the framebuffer is cropped to the dimensions of the primary monitor with --fb 3840x2160x0x0
# the transformation is used to move the secondary display up by half of the
# delta's with 1.5,0,-960.0,0,1.5,0,0,0,1
xrandr --auto --output HDMI-1 --same-as eDP-1 --transform 3.0,0,0,0,3.0,-120.0,0,0,1 --fb 3840x2160x0x0
```