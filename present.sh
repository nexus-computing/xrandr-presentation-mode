#!/bin/bash
cd `dirname $0`

USAGE="\n
usage: present.sh <primary display> <secondary display>\n
\n
present.sh: Mirrors the primary display with the appropriate projection onto the secondary display.\n
\n
for a list of displays use xrandr\n
\n
examples usage:\n
present.sh eDP-1 HDMI-1\n
"


if [ "$#" -ne 2 ]; then
    echo -e $USAGE
    exit -1
fi

PRIMARY=$1
SECONDARY=$2

TRANSFORM=`./project.py $PRIMARY $SECONDARY`
if [ $? -ne 0 ]; then
    echo $TRANSFORM
    exit -2
fi

xrandr --output $SECONDARY --auto $TRANSFORM